<?php
require "dbConnect.php";
$name = $address = $salary = $mobile = $email = $gender = $empId =  "" ;
$nameErr = $emailErr = $empIdErr =   $fileErr = "";


 if($_SERVER["REQUEST_METHOD"] == "POST"){

        $sql = "SELECT id FROM empDetails WHERE employeeId = ?";
        
        if($stmt = $conn->prepare($sql)){
            $stmt->bind_param("i", $param_empId);
            $param_empId = trim($_POST["empId"]);
            if($stmt->execute()){
                // store result
                $stmt->store_result();
                
                if($stmt->num_rows == 1){
                    $empIdErr = "Employee record already exist.";
                } else{
                    $empId = trim($_POST["empId"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

           
            $stmt->close();
        }
    
        
 
            $fname = $_FILES['file']['name'];
            $target_dir = "";
            $target_file = $target_dir . basename($_FILES["file"]["name"]);
          
            
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          
           
            $extensions_arr = array("jpg","jpeg","png","gif");
            if( in_array($imageFileType,$extensions_arr) ){
                $fileName = $fname;
                move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);
            
            }
            else
            {
                $fileErr = "File format not supported";
            }
 
        
            $name = trim($_POST["name"]);
            if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
                $nameErr = "Only letters and white space allowed";
              }

            $gender = $_POST["gender"];
            $mobile = trim($_POST["mobile"]);
            $email = trim($_POST["email"]);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Invalid email format";
            }
            $address = trim($_POST["address"]);
            
            
 
          
        
    
      
    if(empty($nameErr) && empty($emailErr) && empty($empIdErr) && empty($fileErr)) {

        $sql = "INSERT INTO empDetails (name, employeeId, gender,mobile, email, address,image) VALUES (?, ?, ?, ?, ?, ?, ?)";
 
        if($stmt = $conn->prepare($sql)){
            $stmt->bind_param("sisssss", $name, $empId, $gender, $mobile, $email, $address, $fileName);
            move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);
            
            
             if($stmt->execute()){

                header("location: home.php");
        
                exit();
            } else{
                echo "Something went wrong here. Please try again later.";
               
            }
        }
    
        $stmt->close();
    }
    
    $conn->close();
}




?>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style>
        

        .error {color: red;}
        
        form {
    width: 300px;
    margin: 0 auto;
}
    </style>
</head>
<body>
<a href="home.php" class="btn btn-info btn-lg pull-right">  
          <span class="glyphicon glyphicon-home"></span> Home
</a>
<div class="page-header">
                        <h1>Employee Details</h1>
                    </div>
                          


<div class="wrapper">

        <p><span class="error"></span></p>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype='multipart/form-data'>  
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control" required>
                <span class="error"> <?php echo $nameErr;?></span>
                
            </div>    
            <div class="form-group">
                <label>Employee ID</label>
                <input type="text" name="empId" class="form-control" required>
                <span class="error"> <?php echo $empIdErr;?></span>
            </div>
            <div class="form-group">
                <label>Gender</label>
                <label class="radio-inline"><input type="radio" name="gender" value="Male" required>Male</label>
                <label class="radio-inline"><input type="radio" name="gender" value="Female">Female</label>
                <label class="radio-inline"><input type="radio" name="gender" value="Other">Other</label>
            </div>
            <div class="">
                <label>Upload Photo</label>
                <input type="file" name="file" class="form-control" required>
                <span class="error"> <?php echo $fileErr;?></span>

            </div>
            <div class="form-group">
                <label>Mobile</label>
                <input type="tel" name="mobile" class="form-control" required>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control" required>
                <span class="error"> <?php echo $emailErr;?></span>
            </div>
            <div class="form-group">
                <label>Address</label>
                <textarea name="address" rows="5" cols="40"  class="form-control" required></textarea>               
            </div>

            

            <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
            <input type="submit" name="submit" class="btn btn-primary" value="Submit">  
            <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </form>
    </div>    



</body>
</html>