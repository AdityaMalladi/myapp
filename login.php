

<?php
/*
// Initialize the session
session_start();
 
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: welcome.php");
    exit;
}
 
// Include config file
require_once "dbConnect.php";
 
// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter username.";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT id, name, password FROM users WHERE username = ?";
        
        if($stmt = $conn->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bind_param("s", $param_username);
            
            // Set parameters
            $param_username = $username;
            
            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // Store result
                $stmt->store_result();
                
                // Check if username exists, if yes then verify password
                if($stmt->num_rows == 1){                    
                    // Bind result variables
                    $stmt->bind_result($id, $username, $hashed_password);
                    if($stmt->fetch()){
                        if(password_verify($password, $hashed_password)){
                            // Password is correct, so start a new session
                            session_start();
                            
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                            // Redirect user to welcome page
                            header("location: home.php");
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "No account found with that username.";
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            $stmt->close();
        }
    }
    
    // Close connection
    $conn->close();
}*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <style type="text/css">
    .error {color: red;}
        body{ font: 14px sans-serif; }
        .wrapper{ 
            
            padding-top: 125px;
            width: 500px;
            margin: 0 auto;

            }
    </style>
</head>
<body>
    <div id = "msg"></div>
    <div class="wrapper">
        <h2>Login</h2>
        <p>Please fill in your credentials to login.</p>
        <p><span class="error"></span></p>
        <form action="#" method="post">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" id="name" class="form-control">
                <span class = "error" id="nameErr"></span>
                <br>
                
            </div>    
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" id="password"class="form-control" required>
                <span class="error" id="passwordErr"> </span>

            </div>
            <div class="form-group">
                <input type="button" class="btn btn-primary" id="login" value="Login">
                <a href="form.php" class="btn btn-primary">Register</a>
            </div>
            <p>Don't have an account? <a href="register.php">Sign up now</a>.</p>
        </form>
    </div>    
</body>
</html>

<script>


    $(document).ready(function() {
       
       



    $("#login").click(function() {
        var nameErr;
        var passwordErr;
        var name = $("#name").val();
        var password = $("#password").val();
    if (name == '') {
        $("#nameErr").html("Username required");
    }
    else if(password == ''){
        $("#passwordErr").html("Password required");
    }
    else{
        $.ajax({
            url: "check.php",
            type: "post",
            data: {name: name,
                   password: password
                },
            success: function(response){
                if(response == "success"){
                    
                    window.location = "home.php";
                }
                else{
                   // $('#msg').html(response);
                   alert(response);
                }
            }



        })
    }
})
    })
</script>