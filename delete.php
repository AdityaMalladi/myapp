<?php
    if(isset($_POST['id']) && !empty($_POST['id'])){
        require "dbConnect.php";
        $sql = "DELETE FROM empDetails WHERE id = ?";
        $id = $_POST["id"];
        if($stmt = $conn->prepare($sql))
        {
            $stmt->bind_param("i",$id );
            if($stmt->execute()){
                header("location: home.php");
                exit();
            }
            else{
                echo "Something wrong";
            }
        }
        $stmt->close();
        $conn->close();
    }
    

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style>
        .a{
            width: 500px;
            margin: 0 auto;
        }
    </style>
    <title>Delete</title>
</head>
<body>
<a href="home.php" class="btn btn-info btn-lg pull-right">  
          <span class="glyphicon glyphicon-home"></span> Home
</a>

<div class="page-header">
                        <h1>Employee Details</h1>
                    </div>
    <div class = "container">
    <form action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
    <input type = "hidden" name = "id" value = "<?php echo trim($_GET['id']);?>">
    
    <h1> Are you sure to delete details of employee with ID: <?php echo trim($_GET['id']); ?> ?</h1>
    
    <input type = "submit" value = "Yes" class = "btn btn-danger">
    <a href="home.php" class="btn btn-default">No</a>
    
   
     
               
    
    </form>
    </div>
</body>
</html>