<?php
    if(isset($_GET['id']) && !empty(trim($_GET['id']))){
        require "dbConnect.php";
        $sql = "SELECT * FROM empDetails WHERE id = ?";
        if($stmt = $conn->prepare($sql)){
            $id = $_GET['id'];
            $stmt->bind_param('i',$id);
            if($stmt->execute()){
                $result = $stmt->get_result();

                if($result->num_rows == 1)
                $row = $result->fetch_array(MYSQLI_ASSOC);
                $name = $row['name'];
                $empId = $row['employeeId'];
                $gender = $row['gender'];
                $mobile = $row['mobile'];
                $email = $row['email'];
                $address = $row['address'];
                $image = $row['image'];
                $image_src = "".$image;
            
            }
            else{
                header("error.php");
                echo "OOPS !.. Something wrong";
            }
            $stmt->close();
        }
        $conn->close();
    }
    else
    {
        header("error.php");
        exit();
    }
   


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Read</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    
    <style>
        #leftbox { 
                float:left;  
                
                width:500px;
                margin: auto;
               padding-top: 20px;
                padding-left: 350px;
            }
            #rightbox{ 
                float:right; 
                padding-right: 40px;
                font-size: 20px;
                padding-top:30px;
                width:800px;
  margin: auto;
                 
             
            } 
     #box{
         padding-top: 45px;
     }
    </style>
</head>
<body>

<a href="home.php" class="btn btn-info btn-lg pull-right">  
          <span class="glyphicon glyphicon-home"></span> Home
</a>
<div class="page-header">
                        <h1>Employee Details</h1>
    </div>
             
<div id = box>       </div>
     <div id=leftbox>
     <img src='<?php echo $image_src; ?>'style="width:250px;height:250px;">
</div>
     
    <div id="rightbox">
    <p> Name:<b><?php echo $row['name']; ?></b></p>
    <p>Employee ID:<b><?php echo $row['employeeId']; ?></b></p>
     <p>Gender:<b><?php echo $row['gender']; ?></b> </p>
     <p>Mobile:<b><?php echo $row['mobile']; ?></b> </p>
     <p>Email:<b><?php echo $row['email']; ?></b> </p>
     <p>Address:<b><?php echo $row['address']; ?></b> </p>
     
</div>
</div>
    

</body>
</html>