<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require "dbConnect.php";
 
$name = $address = $salary = $mobile = $email = $gender = $empId =  "" ;
$nameErr = $emailErr =  $empIdErr = $fileErr = "";
 

if(isset($_POST["id"]) && !empty($_POST["id"])){
    $id = $_POST["id"];
    
    
        $name = $_POST["name"];
        if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            $nameErr = "Only letters and white space allowed";
          }

          $fname = $_FILES['file']['name'];
          $target_dir = "uploads/";
          $target_file = $target_dir . basename($_FILES["file"]["name"]);
        
          
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        
         
          $extensions_arr = array("jpg","jpeg","png","gif");
          if( in_array($imageFileType,$extensions_arr) ){
              $fileName = $fname;
              move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);
          
          }
          else
          {
              $fileErr = $fname;
          }
    
        $mobile = $_POST["mobile"];
    
    
    
        $email = $_POST["email"];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
        }
        $gender = $_POST["gender"];
        $empId = $_POST["empId"];
        $address = $_POST["address"];
        if(empty($nameErr) && empty($emailErr) && empty($fileErr)){
            $sql = "UPDATE empDetails SET name=?, employeeId = ?, gender = ?, mobile=?, email=?, address = ?,image = ? WHERE id=?";
     
            if($stmt = $conn->prepare($sql)){
                $stmt->bind_param("sisssssi", $name,  $empId,  $gender,  $mobile,  $email,  $address, $fileName, $id);
                
                if($stmt->execute()){
                    echo '<script>  
                         alert("Updated successfully"); 
                         </script>' ;
                    header("location: home.php");
                    exit();
                } else{
                    echo "Something went here  wrong. Please try again later.";
                    echo '<script>  
                         alert("duplicate one"); 
                         </script>' ; 
                }
            }
             
            $stmt->close();
        }
        
        $conn->close();
    }
    else{
        if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
            $id =  trim($_GET["id"]);
            
            $sql = "SELECT * FROM empDetails WHERE id = ?";
            if($stmt = $conn->prepare($sql)){
                $stmt->bind_param("i", $param_id);
                
                $param_id = $id;
                
                if($stmt->execute()){
                    $result = $stmt->get_result();
                    
                    if($result->num_rows == 1){
                        $row = $result->fetch_array(MYSQLI_ASSOC);
                        
                        $name = $row["name"];
                        $empId = $row["employeeId"];
                        $gender = $row["gender"];
                        $name = $row["name"];
                        $mobile = $row["mobile"];
                        $email = $row["email"];
                        $address = $row["address"];
                        $fileName = $row["image"];
                    } else{
                        header("location: view.php");
                        exit();
                    }
                    
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
            
            $stmt->close();
            
            $conn->close();
        }  else{
            header("location: home.php");
            exit();
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style>
        

        .error {color: red;}
        
        form {
    width: 300px;
    margin: 0 auto;
}
    </style>
    
</head>
<body>
<a href="home.php" class="btn btn-info btn-lg pull-right">  
          <span class="glyphicon glyphicon-home"></span> Home
</a>
<div>
<div class="page-header">
                        <h1>Employee Details</h1>
                    </div>
                    
<p><span class="error"></span></p>
<form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post" enctype='multipart/form-data'>
  Name: <input type="text" name="name" class="form-control" value="<?php echo $name?>" required>
  <span class="error"> <?php echo $nameErr;?></span>
  <br><br>
  Emp Id: <input type="text" name="empId" class="form-control" value="<?php echo $empId?>" required>
  <span class="error"> <?php echo $empIdErr;?></span>
  <br><br>
  Gender:
  <label class="radio-inline"><input type="radio" name="gender" value="Male" required>Male</label>
  <label class="radio-inline"><input type="radio" name="gender" value="Female">Female</label>
  <label class="radio-inline"><input type="radio" name="gender" value="Other">Other</label>
  <br><br>
  Mobile: <input type="tel" name="mobile" class="form-control" value="<?php echo $mobile?>" required>
  <br><br>
  <label>Upload Photo</label>
                <input type="file" name="file" class="form-control" required>
                <span class="error"> <?php echo $fileErr;?></span>
  <br><br>
  E-mail: <input type="email" name="email" class="form-control" value="<?php echo $email?>" required>
  <span class="error"> <?php echo $emailErr;?></span>
  <br><br>
  Address: <input type = "text" name="address" rows="5" cols="40"  class="form-control" value="<?php echo $address?>" required></input>
  
  <br><br>
  <input type="hidden" name="id" value="<?php echo $id; ?>"/>
  <input type="submit" name="Update" class="btn btn-primary" value="Submit">  
  <input type="reset" class="btn btn-default" value="Reset">
</form>


    
</body>
</html>