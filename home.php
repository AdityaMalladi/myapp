<?php
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>


    <style>
        .container{
                padding-top: 60px;
        }
        
    </style>
</head>
<div class="page-header">
    <h1>Employee Details</h1>

    <div align="right">                         
  <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?php echo htmlspecialchars($_SESSION["username"]); ?> <span class="glyphicon glyphicon-user"></span>
    <span class="caret"></span></button>
    <ul class="dropdown-menu dropdown-menu-right">
      <li><a href="logout.php">Sign out</a></li>
      
    </ul>
  </div>
</div>
    </div>


    
<a href="create.php" class="btn btn-info btn-lg pull-right">Add new employee</a>

<div class="container">
    
    <?php
    error_reporting(E_ERROR | E_WARNING | E_PARSE);
    $results_per_page = 5;
    if (isset($_GET["page"])) { 
        $page  = $_GET["page"]; 
    } 
    else {
        $page=1; 
    }
    $start = ($page-1) * $results_per_page;
        
    require "dbConnect.php";

    $sql = "SELECT *  FROM empdetails ORDER BY id ASC LIMIT $start, $results_per_page";
    if($result = $conn->query($sql)){
        if($result->num_rows > 0){
            echo "<table class='table table-hover table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            //echo "<tr>";
                echo "<th>#</th>";
                echo "<th>Name</th>";
                echo "<th>Emp ID</th>";
                echo "<th>Actions</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            while($row = $result->fetch_array()){
                echo "<tr>";
                    echo "<td>".$row['id']."</td>";
                    echo "<td>".$row['name']."</td>";
                    echo "<td>".$row['employeeId']."</td>";
                    echo "<td>";
                    echo "<a href='view.php?id=". $row['id'] ."' title='View Record' ><span class='glyphicon glyphicon-eye-open'></span></a>";
                    echo "&nbsp&nbsp&nbsp";
                    echo "<a href='update.php?id=". $row['id'] ."' title='Update Record' ><span class='glyphicon glyphicon-pencil'></span></a>";
                    echo "&nbsp&nbsp&nbsp";
                    echo "<a href='delete.php?id=". $row['id'] ."' title='Delete Record' ><span class='glyphicon glyphicon-trash'></span></a>";
                    echo "</td>";
                echo "<tr>";
            }
            echo "</tbody>";
            echo "<table>";
            echo "<div>";
        $result->free();
        }
        else{
        echo "No records";
        }
    }
    else{
    echo "Couldn't execure sql";
}
    
$sql = "SELECT COUNT(id) AS total FROM empdetails";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$total_pages = ceil($row["total"] / $results_per_page); 
for ($i=1; $i<=$total_pages; $i++) { 
    echo "<div class='center'>";
    echo "<ul class='pagination'>";
    if ($i == $page) {
        echo "<li class = 'active' ><a href='home.php?page=".$i."'"; 
        echo ">$i</a></li>";
        echo "</ul>";
     }
     else{
    echo "<li ><a href='home.php?page=".$i."'";
    echo ">$i</a></li>";
    echo "</ul>";
     }
    echo "</div";
}

$conn->close();
?>
</div>
</body>
</html>